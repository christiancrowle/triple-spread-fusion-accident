#include <vadefs.h>
#include <Windows.h>

#include <process_memory.h>

process_memory::process_memory (unsigned long pid) : pid(pid) {
    this->handle = OpenProcess(PROCESS_VM_READ | PROCESS_VM_WRITE | PROCESS_VM_OPERATION, FALSE, pid);
}

size_t process_memory::readbuf(uintptr_t addr, unsigned char *buf, size_t count) {
    size_t read = 0;
    int result = ReadProcessMemory(this->handle, (void*)addr, buf, count, &read);

    if (!result) read = -1;

    return read;
}

size_t process_memory::writebuf(uintptr_t addr, unsigned char *buf, size_t count) {
    size_t written = 0;
    size_t result = WriteProcessMemory(this->handle, (void*)addr, buf, count, &written);

    if (!result) written = -1;

    return written;
}

unsigned char *process_memory::alloc_and_readbuf(uintptr_t addr, size_t count) {
    unsigned char *buf = (unsigned char*)malloc(count);
    size_t result = this->readbuf(addr, buf, count);

    if (result == -1) {
        free(buf);
        buf = nullptr;
    }
    return buf;
}

