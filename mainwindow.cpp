#include <Windows.h>
#include <psapi.h>

#include <QMessageBox>

#include "mainwindow.h"
#include "./ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow) {
    ui->setupUi(this);

    connect(ui->btnScan, &QPushButton::released, this, &MainWindow::btnScanReleased);
    connect(ui->btnAttach, &QPushButton::released, this, &MainWindow::btnAttachReleased);
    connect(ui->btnRead, &QPushButton::released, this, &MainWindow::btnReadReleased);
    connect(ui->btnWrite, &QPushButton::released, this, &MainWindow::btnWriteReleased);
    connect(ui->btnSave, &QPushButton::released, this, &MainWindow::btnSaveReleased);
    connect(ui->btnLoad, &QPushButton::released, this, &MainWindow::btnLoadReleased);
}

MainWindow::~MainWindow() {
    delete ui;
}

void MainWindow::btnScanReleased() {
    // get the first 1024 processes
    unsigned long pids[1024];
    int processes_len;
    unsigned long bytes_needed;

    if (!EnumProcesses(pids, sizeof(pids), &bytes_needed)) {
        qDebug() << "uh oh! failed to enumerate! will probably crash now.";
    }

    processes_len = bytes_needed / sizeof(unsigned long);
    for (int i = 0; i < processes_len; i++) {
        qDebug() << "PID: " << pids[i];
    }

    // clear the process list
    ui->processComboBox->clear();

    // now we should get the image name from each process, and then add it to our list
    for (int i = 0; i < processes_len; i++) {
        TCHAR process_name[MAX_PATH] = TEXT("!!! idklol.exe");
        void *process_handle = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, pids[i]);

        if (process_handle != nullptr) {
            HMODULE module_handle;
            unsigned long size;
            if (EnumProcessModulesEx(process_handle, &module_handle, sizeof(module_handle), &size, 0)) {
                GetModuleBaseName(process_handle, module_handle, process_name, sizeof(process_name)/sizeof(TCHAR));
            }
        }

        QString process_list_entry = QString::fromWCharArray(process_name, sizeof(process_name)/sizeof(TCHAR));
        process_list_entry.append(" // ");
        process_list_entry.append(QString::number(pids[i]));

        ui->processComboBox->addItem(process_list_entry, QVariant::fromValue(pids[i]));
        processes_map.emplace(pids[i], std::wstring(process_name));

        CloseHandle(process_handle);
    }

    ui->processComboBox->model()->sort(0, Qt::AscendingOrder);
}

void MainWindow::btnAttachReleased() {
    QVariant selected_process_variant = ui->processComboBox->currentData();
    if (selected_process_variant.isValid()) {
        unsigned long pid = selected_process_variant.toUInt();
        this->current_process = new process_memory(pid);
        qDebug() << "attached to process w/ pid " << pid << "!!";
    } else {
        qWarning() << "selected process variant invalid.";
    }
}

void MainWindow::btnReadReleased() {
    QMessageBox msgBox;
    QString addr_s = ui->addrTextBox->text();
    uintptr_t addr = strtoull(addr_s.toStdString().c_str(), nullptr, 0);

    if (!addr) {
        msgBox.setText("I assume you probably don't want to deref a null pointer. Check your addr!!");
        msgBox.exec();
        return;
    }

    if (!this->current_process) {
        msgBox.setText("Not attached!!");
        msgBox.exec();
        return;
    }

    uint32_t data = this->current_process->read<uint32_t>(addr);
    QString hex = QString("0x%1").arg(data, 8, 16, QLatin1Char('0'));

    ui->currentValueTextBox->setText(hex);
}

void MainWindow::btnWriteReleased() {
    QMessageBox msgBox;
    QString addr_s = ui->addrTextBox->text();
    uintptr_t addr = strtoull(addr_s.toStdString().c_str(), nullptr, 0);

    if (!addr) {
        msgBox.setText("I assume you probably don't want to deref a null pointer. Check your addr!!");
        msgBox.exec();
        return;
    }

    if (!this->current_process) {
        msgBox.setText("Not attached!!");
        msgBox.exec();
        return;
    }

    QString data_s = ui->currentValueTextBox->text();
    uint32_t data = strtoul(data_s.toStdString().c_str(), nullptr, 0);
    qDebug() << data;
    this->current_process->write<uint32_t>(addr, data);
}

void MainWindow::btnSaveReleased() {

}

void MainWindow::btnLoadReleased() {

}
