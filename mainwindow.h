#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <process_memory.h>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void btnScanReleased();
    void btnAttachReleased();
    void btnReadReleased();
    void btnWriteReleased();
    void btnSaveReleased();
    void btnLoadReleased();

private:
    Ui::MainWindow *ui;
    std::map<unsigned long, std::wstring> processes_map;
    process_memory *current_process;
};
#endif // MAINWINDOW_H
