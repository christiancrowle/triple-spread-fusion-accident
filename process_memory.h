#ifndef UTIL_H
#define UTIL_H

#include <vadefs.h>
#include <type_traits>

class process_memory {
public:
    process_memory(unsigned long pid);
    size_t readbuf(uintptr_t addr, unsigned char *buf, size_t count);
    unsigned char *alloc_and_readbuf(uintptr_t addr, size_t count);

    size_t writebuf(uintptr_t addr, unsigned char *buf, size_t count);

    template <typename T,
              typename = typename std::enable_if<std::is_arithmetic<T>::value, T>::type>
    T read(uintptr_t addr) {
        unsigned char *data = this->alloc_and_readbuf(addr, sizeof(T));
        // fixme: handle failure (look for null)
        T result = *((T*)data);
        free(data);

        return result;
    }

    template <typename T,
              typename = typename std::enable_if<std::is_arithmetic<T>::value, T>::type>
    T write(uintptr_t addr, T value) {
        unsigned char *buf = (unsigned char*)malloc(sizeof(T));
        memcpy(buf, &value, sizeof(T));

        size_t result = this->writebuf(addr, buf, sizeof(T));
        free(buf);

        return result;
    }
private:
    void *handle;
    unsigned long pid;
};

#endif // UTIL_H
